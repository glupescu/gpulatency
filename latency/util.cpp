/* Various constructs/helpers */
#include "util.h"

/*******************************************************
*	Function: timers
*******************************************************/

void PROFILER::start()
{
	clock_gettime(CLOCK_REALTIME, &time_start);
}

#define REALTIME_PROFILER 1

#ifdef REALTIME_PROFILER
void PROFILER::stop(const char* msg)
{
	clock_gettime(CLOCK_REALTIME, &time_end);
    time_diff = ((time_end.tv_sec * 1000000000) + time_end.tv_nsec)
				- ((time_start.tv_sec * 1000000000) + time_start.tv_nsec);
	time_diff = time_diff / 1000000;
	cout << "[ " << time_diff << " ms] " << msg << endl;
}
#else
void PROFILER::stop(const char* msg)
{
	clock_gettime(CLOCK_REALTIME, &time_end);
    time_diff = ((time_end.tv_sec * 1000000000) + time_end.tv_nsec)
				- ((time_start.tv_sec * 1000000000) + time_start.tv_nsec);
	time_diff = time_diff / 1000000;
	time_log << "[ " << time_diff << " ms] " << msg << endl;
}
#endif

void PROFILER::show_log()
{
	cout << endl << " ------- PROFILER LOG -------- " << endl;
	cout << time_log.str();
	cout << endl;
}

/*******************************************************
*	Function: io_parse_options
*******************************************************/
void user_parse_options(int argc, char** argv, ARGV_OPTIONS* argv_options)
{
	char argId;

	/* defaults */
	argv_options->dev_id		= 0;
	argv_options->dev_query	= false;
	
	while (1) {
        int option_index = 0;
        static struct option long_options[] = {
			{"dev", 	required_argument,  0,  'd'},
            {"qry",		no_argument,		0,  'q'},
			{"help", 	no_argument,  		0,  'h'},
            {0,    		0,      			0, 	  0}
        };

		argId = getopt_long(argc, argv, "qdh", long_options, &option_index);
        if (argId == -1)
            break;

        switch (argId) {
        case 'q':
    	   argv_options->dev_query = true;
           break;
        case 'd':
           argv_options->dev_id = atoi(optarg);
           break;
		   
		case 'h':
			cout << " ACCOPT, @2014" << endl << endl;
			cout << " --dev [devid] \t\t\t select OpenCL device id " << endl << endl;
			cout << " --qry \t\t\t query OpenCL devices " << endl;
			cout << " --help  \t\t\t display help " << endl;
			exit(0);
        	break;

       default:
           printf("?? getopt returned character code 0%o ??\n", argId);
        }
    }
}

/*******************************************************
* FUNCTION: print_cl_errstring
* check/print error codes
*******************************************************/
const char* cl_decode_error(cl_int err) {
	switch (err) {
	case CL_SUCCESS:                     	return  "Success!";
	case CL_DEVICE_NOT_FOUND:               return  "Device not found.";
	case CL_DEVICE_NOT_AVAILABLE:           return  "Device not available";
	case CL_COMPILER_NOT_AVAILABLE:         return  "Compiler not available";
	case CL_MEM_OBJECT_ALLOCATION_FAILURE:  return  "Memory object alloc fail";
	case CL_OUT_OF_RESOURCES:               return  "Out of resources";
	case CL_OUT_OF_HOST_MEMORY:             return  "Out of host memory";
	case CL_PROFILING_INFO_NOT_AVAILABLE:   return  "Profiling information N/A";
	case CL_MEM_COPY_OVERLAP:               return  "Memory copy overlap";
	case CL_IMAGE_FORMAT_MISMATCH:          return  "Image format mismatch";
	case CL_IMAGE_FORMAT_NOT_SUPPORTED:     return  "Image format no support";
	case CL_BUILD_PROGRAM_FAILURE:          return  "Program build failure";
	case CL_MAP_FAILURE:                    return  "Map failure";
	case CL_INVALID_VALUE:                  return  "Invalid value";
	case CL_INVALID_DEVICE_TYPE:            return  "Invalid device type";
	case CL_INVALID_PLATFORM:               return  "Invalid platform";
	case CL_INVALID_DEVICE:                 return  "Invalid device";
	case CL_INVALID_CONTEXT:                return  "Invalid context";
	case CL_INVALID_QUEUE_PROPERTIES:       return  "Invalid queue properties";
	case CL_INVALID_COMMAND_QUEUE:          return  "Invalid command queue";
	case CL_INVALID_HOST_PTR:               return  "Invalid host pointer";
	case CL_INVALID_MEM_OBJECT:             return  "Invalid memory object";
	case CL_INVALID_IMAGE_FORMAT_DESCRIPTOR:return  "Invalid image format desc";
	case CL_INVALID_IMAGE_SIZE:             return  "Invalid image size";
	case CL_INVALID_SAMPLER:                return  "Invalid sampler";
	case CL_INVALID_BINARY:                 return  "Invalid binary";
	case CL_INVALID_BUILD_OPTIONS:          return  "Invalid build options";
	case CL_INVALID_PROGRAM:                return  "Invalid program";
	case CL_INVALID_PROGRAM_EXECUTABLE:     return  "Invalid program exec";
	case CL_INVALID_KERNEL_NAME:            return  "Invalid kernel name";
	case CL_INVALID_KERNEL_DEFINITION:      return  "Invalid kernel definition";
	case CL_INVALID_KERNEL:                 return  "Invalid kernel";
	case CL_INVALID_ARG_INDEX:              return  "Invalid argument index";
	case CL_INVALID_ARG_VALUE:              return  "Invalid argument value";
	case CL_INVALID_ARG_SIZE:               return  "Invalid argument size";
	case CL_INVALID_KERNEL_ARGS:            return  "Invalid kernel arguments";
	case CL_INVALID_WORK_DIMENSION:         return  "Invalid work dimension";
	case CL_INVALID_WORK_GROUP_SIZE:        return  "Invalid work group size";
	case CL_INVALID_WORK_ITEM_SIZE:         return  "Invalid work item size";
	case CL_INVALID_GLOBAL_OFFSET:          return  "Invalid global offset";
	case CL_INVALID_EVENT_WAIT_LIST:        return  "Invalid event wait list";
	case CL_INVALID_EVENT:                  return  "Invalid event";
	case CL_INVALID_OPERATION:              return  "Invalid operation";
	case CL_INVALID_GL_OBJECT:              return  "Invalid OpenGL object";
	case CL_INVALID_BUFFER_SIZE:            return  "Invalid buffer size";
	case CL_INVALID_MIP_LEVEL:              return  "Invalid mip-map level";
	default:                                return  "Unknown";
	}
}

/*******************************************************
* FUNCTION: errorLog
* check/print error codes
*******************************************************/
void cl_compile_log(cl_program program, cl_device_id device)
{
	char* build_log;
	size_t log_size;

	/* first call to know the proper size */
	clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG,
		0, NULL, &log_size);
	build_log = new char[log_size + 1];

	/* Second call to get the log */
	clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG,
		log_size, build_log, NULL);
	build_log[log_size] = '\0';
	
	/* output to stdout */
	cout << build_log;
	
	/* free resources */
	delete[] build_log;
}

/*******************************************************
* FUNCTION: check
* check/print error codes
*******************************************************/
bool cl_check(int cuerr){

	if (cuerr != CL_SUCCESS)
	{
		cout << cl_decode_error(cuerr) << endl;
		return false;
	}
	return true;
}

/*******************************************************
* FUNCTION: getSrcCode
* read kernel source code from kernel.cl file
*******************************************************/
bool user_get_kernel(const char* filename, string& source)
{
	string line;
	ifstream in(filename);
	DIE(in.good() != true, "Kernel file not found");
	
	while (getline(in, line))
		source += line + "\n";
		
	return true;
}


