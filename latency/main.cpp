/* Various constructs/helpers */
#include "util.h"

/*******************************************************
* Global profiler
*******************************************************/

PROFILER profiler;

/*******************************************************
*	Entry point
*******************************************************/
#if defined(WIN32)
int _tmain(int argc, _TCHAR* argv[])
#else 
int main(int argc, char** argv)
#endif
{
	/* struct for holding OpenCL compute related info */
	OCL_GPU ocl_gpu;	/* struct for holding OpenCL compute related info */
	ARGV_OPTIONS argv_options;	/* argv options */
	
	double *vec_in;			/* input vector */
	double *vec_out_ref;	/* output reference vector serial */
	double *vec_out_cl;		/* output vector opencl */
	int vec_len = 16 * 1024 * 1024; /* 32MB */
	double alfa = 2.72;		/* Euler */
	
	/* start profiler */
	profiler.start();

	/* parse user CLI options */
	profiler.stop(" [USER] argv_parse_options");
	user_parse_options(argc, argv, &argv_options);

	/* init OpenCL runtime */
	profiler.stop(" [CL] alg_cl");
	cl_api_exec(&ocl_gpu, vec_in, vec_out_cl, vec_len, alfa, argv_options);
		
	profiler.stop(" [USER] EXIT");
	profiler.show_log();
	
#if defined(WIN32)
	system("PAUSE");
#endif

	return 0;
}
