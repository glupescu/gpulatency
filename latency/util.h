#ifndef UTIL_H
#define UTIL_H

#if defined(WIN32)
#include "stdafx.h"
#endif

/* Common headers C/C++ */
#include <stdio.h>
#include <stdlib.h>
#include <cstdlib>
#include <stdio.h>
#include <string.h>
#include <string>
#include <stdint.h>
#include <math.h>
#include <limits.h>
#include <vector>
#include <string>
#include <memory>
#include <unistd.h>
#include <omp.h>

/* Common headers C++ */
#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include <getopt.h>
#include <sstream>

/* OpenCL headers */
#include "CL/cl.h"

#define MAX_PLATFORMS 2
#define DEFAULT_PLATFORM_CL 0
#define MAX_STR_SIZE 512
#define MAX_DEVICE_COUNT 4
#define OCL_GPU_MAX 1
#define NUM_DEVICES 4

/* chose which platform and device to process on*/
#define SELECT_PLATFORM_ID	0
#define SELECT_DEVICE_ID	0

using namespace std;

/*******************************************************
*	MACROS
*******************************************************/

/* Force exit program on critical error */
#define DIE(assertion, call_description)		\
do {\
if (assertion) { \
		fprintf(stderr, "(%s, %d): ", \
		__FILE__, __LINE__);			\
		perror(call_description);		\
		exit(EXIT_FAILURE);			\
}					\
} while (0)

/* MAX macro */
#define MAX(x, y) ( ((x) > (y)) ? (x) : (y) )


/*******************************************************
*	STRUCT ARGV OPTIONS
*******************************************************/

struct ARGV_OPTIONS
{
	/* user selected device id */
	uint dev_id;
	bool dev_query;
};

/*******************************************************
*	CLASS PROFILER
*******************************************************/

class PROFILER
{
	struct timespec time_start;
	struct timespec time_end;
	unsigned long time_diff;
	stringstream time_log;
	
	public:
		void start();
		void stop(const char* msg);
		void show_log();
};

/*******************************************************
*	STRUCT COMPUTE DEVICE
*******************************************************/
struct OCL_GPU
{
	/* OpenCL kernel related */
	cl_program			dev_program;
	cl_kernel			dev_kernel_exec;
	cl_context			dev_context;
	cl_command_queue 	dev_cmd_queue;
	cl_device_id		dev_id;
	cl_platform_id		dev_platform;

	/* OpenCL buffers */
	cl_mem	vec_in;
	cl_mem	vec_out;

	/* OpenCL info */
	string info_name;
	cl_int info_type;
};


using namespace std;

/*******************************************************
*	FUNCTION DEFINITIONS ALG OPENCL
*******************************************************/


bool user_get_kernel(const char* filename, std::string& source);
bool user_init_data(double** vec_in, double** vec_out, int vec_len);
void user_parse_options(int argc, char** argv, ARGV_OPTIONS* argv_options);

bool cl_check(int cuerr);
const char* cl_decode_error(cl_int err);
void cl_compile_log(cl_program program, cl_device_id device);
bool cl_api_exec(OCL_GPU* ocl_gpu, double* vec_in, double* vec_out, 
		int vec_len, double alfa, ARGV_OPTIONS argv_options);
bool cl_api_exec_mp(OCL_GPU* ocl_gpu, double* vec_in, double* vec_out, 
		int vec_len, double alfa, ARGV_OPTIONS argv_options);


#endif
