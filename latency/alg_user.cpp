/* Various constructs/helpers */
#include "util.h"

/*******************************************************
* Global profiler
*******************************************************/

extern PROFILER profiler;

/*******************************************************
* FUNCTION: init_data
* handles OpenCL initialization (read kernel, compile...)
*******************************************************/

bool user_init_data(double** vec_in, 
			double** vec_out,
			int vec_len)
{
	*vec_in = new double[vec_len];
	DIE(*vec_in == NULL, "Could not allocate vec_in");
	
	*vec_out = new double[vec_len];
	DIE(*vec_out == NULL, "Could not allocate vec_out_cl");
	
	for(int i=0; i<vec_len; i++){
		(*vec_in)[i] = i % 1000 + 0.12 * i;
		(*vec_out)[i] = 0;
	}
	
	return true;
}

/*******************************************************
* FUNCTION: cpu_exec
* initial CPU non-parallel code execution
*******************************************************/
bool alg_ref_exec(double* vec_in, 
		double* vec_out, 
		int vec_len, 
		double alfa)
{
	for(int i=0; i<vec_len; i++)
		vec_out[i] = cos(alfa* vec_in[i]);

	return true;
}

/*******************************************************
*	Check vector
*******************************************************/

bool alg_valid_output(double* vec_out_ref, 
				double* vec_out_cl, 
				int vec_len)
{
	for(int i=0; i < vec_len; i++)
		if(vec_out_ref[i] != vec_out_cl[i]){
			cout << "First difference at " << i 
					<< ", " << vec_out_ref[i] <<" != " << 
						vec_out_cl[i] << endl;
			return false;
	}
	return true;
}
