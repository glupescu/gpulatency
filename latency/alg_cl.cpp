/* Various constructs/helpers */
#include "util.h"

/*******************************************************
* Global profiler
*******************************************************/

extern PROFILER profiler;

/*******************************************************
* FUNCTION: cl_api_exec
* handles OpenCL normal execution
*******************************************************/
bool cl_api_exec(OCL_GPU* ocl_gpu, 
		double* vec_in, 
		double* vec_out, 
		int vec_len, 
		double alfa,
		ARGV_OPTIONS argv_options)
{
	cl_int err;
	cl_int err_compile;
	const char* cstr_source = NULL;
	string source;

	/* distribution/mapping of work */
	size_t global_work;
	double retVal;

	cl_device_id cl_devices[NUM_DEVICES];
	cl_platform_id cl_platforms[MAX_PLATFORMS];
	cl_uint cl_num_devices;
	char clStrInfo[MAX_STR_SIZE];
	cl_uint cl_num_platforms;
	cl_num_devices = 0;

	/* used to retrieve SIMD count of CPU/GPU */
	cl_long clDeviceMaxComputeUnits;

	/* get all OpenCL platforms*/
	profiler.stop(" [CL] clGetPlatformIDs");
	clGetPlatformIDs(MAX_PLATFORMS, cl_platforms, &cl_num_platforms);;

	/* get platform info */
	DIE(SELECT_PLATFORM_ID > cl_num_platforms, "invalid platform selected");
	clGetPlatformInfo(cl_platforms[SELECT_PLATFORM_ID],
		CL_PLATFORM_NAME,
		sizeof(char)* MAX_STR_SIZE,
		clStrInfo, NULL);

	/* get device pointers, filter by GPU type */
	profiler.stop(" [CL] clGetDeviceIDs");
	clGetDeviceIDs(cl_platforms[SELECT_PLATFORM_ID],
		CL_DEVICE_TYPE_CPU,
		MAX_DEVICE_COUNT,
		cl_devices,
		&cl_num_devices);

	/* GPU id selected */
	DIE(SELECT_DEVICE_ID > cl_num_devices, "invalid device selected");
	ocl_gpu->dev_id = cl_devices[SELECT_DEVICE_ID];

	/* save device name */
	profiler.stop(" [CL] clGetDeviceInfo");
	clGetDeviceInfo(ocl_gpu->dev_id, CL_DEVICE_NAME,
		sizeof(char)* MAX_STR_SIZE, clStrInfo, NULL);

	printf(" [%s] ", clStrInfo);
	ocl_gpu->info_name = clStrInfo;

	/* get info on architecture MAX_COMPUTE_UNITS */
	clGetDeviceInfo(ocl_gpu->dev_id,
		CL_DEVICE_MAX_COMPUTE_UNITS,
		sizeof(cl_long), &clDeviceMaxComputeUnits, NULL);
	printf("[CU %d]\n", clDeviceMaxComputeUnits);

	/* create context for current gpu */
	profiler.stop(" [CL] clCreateContext");
	ocl_gpu->dev_context = clCreateContext(NULL, 1,
		&ocl_gpu->dev_id, NULL, NULL, NULL);

	/* create queue for current gpu */
	profiler.stop(" [CL] clCreateCommandQueue");
	ocl_gpu->dev_cmd_queue = clCreateCommandQueue(
		ocl_gpu->dev_context,
		ocl_gpu->dev_id, 0, NULL);

	/* build OPENCL program against current device */
	profiler.stop(" [USER] user_get_kernel");
	user_get_kernel("kernel.cl", source);
	cstr_source = source.c_str();

	/* display kernel code */
	if (false){
		cout << " --------- BEGIN kernel.cl ----------- " << endl;
		cout << source << endl;
		cout << " --------- END kernel.cl ----------- " << endl;
	}

	profiler.stop(" [CL] clCreateProgramWithSource");
	ocl_gpu->dev_program = clCreateProgramWithSource(
		ocl_gpu->dev_context, 1, &cstr_source, NULL, &err_compile);
	if(cl_check(err_compile) != true)
		cl_compile_log(ocl_gpu->dev_program, ocl_gpu->dev_id);

	/* compile OpenCL kernel against current GPU */
	profiler.stop(" [CL] clBuildProgram");
	cl_check(clBuildProgram(ocl_gpu->dev_program, 1,
		&ocl_gpu->dev_id, NULL, NULL, NULL));

	/* select compiled kernel exec*/
	profiler.stop(" [CL] clCreateKernel");
	ocl_gpu->dev_kernel_exec = clCreateKernel(
		ocl_gpu->dev_program, "opencl_exec", &err);	
	if(cl_check(err_compile) != true)
		cl_compile_log(ocl_gpu->dev_program, ocl_gpu->dev_id);

	/* allocate memory data in*/
	profiler.stop(" [CL] clCreateBuffer - vec_in");
	ocl_gpu->vec_in = clCreateBuffer(ocl_gpu->dev_context,
		CL_MEM_READ_WRITE,
		vec_len *sizeof(double), NULL, NULL);
	profiler.stop(" [CL] clCreateBuffer - vec_out");
	ocl_gpu->vec_out = clCreateBuffer(ocl_gpu->dev_context,
		CL_MEM_READ_WRITE,
		vec_len *sizeof(double), NULL, NULL);

	/* init problem data*/	
	profiler.stop(" [USER] user_init_data");
	user_init_data(&vec_in, &vec_out, vec_len);
		
	/* set return value */
	profiler.stop(" [CL] clEnqueueWriteBuffer - vec_in");
	cl_check(clEnqueueWriteBuffer(ocl_gpu->dev_cmd_queue,
		ocl_gpu->vec_in, CL_TRUE,
		0, vec_len * sizeof(double), &vec_in[0], 0, 0, 0));

	/* Set exec kernel arguments */
	profiler.stop(" [CL] clSetKernelArg");
	cl_check(clSetKernelArg(ocl_gpu->dev_kernel_exec, 0,
		sizeof(cl_mem), (void*)&ocl_gpu->vec_in));
	cl_check(clSetKernelArg(ocl_gpu->dev_kernel_exec, 1,
		sizeof(cl_mem), (void*)&ocl_gpu->vec_out));
	cl_check(clSetKernelArg(ocl_gpu->dev_kernel_exec, 2,
		sizeof(cl_double), (void *)&alfa));
		
	/* inital mapping */
	global_work = vec_len; 

	/* enqueue OpenCL exec*/
	profiler.stop(" [CL] clEnqueueNDRangeKernel");
	cl_check(clEnqueueNDRangeKernel(ocl_gpu->dev_cmd_queue,
		ocl_gpu->dev_kernel_exec, 1, NULL, &global_work,
		0, 0, NULL, NULL));

	/* set return value */
	profiler.stop(" [CL] clEnqueueReadBuffer - vec_out");
	cl_check(clEnqueueReadBuffer(ocl_gpu->dev_cmd_queue,
		ocl_gpu->vec_out, CL_TRUE,
		0, vec_len * sizeof(double), &vec_out[0], 0, NULL, NULL));

	/* free target side memory */
	profiler.stop(" [CL] start cleanup");
	clReleaseKernel(ocl_gpu->dev_kernel_exec);
	clReleaseKernel(ocl_gpu->dev_kernel_exec);
	clReleaseProgram(ocl_gpu->dev_program);
	clReleaseMemObject(ocl_gpu->vec_in);
	clReleaseMemObject(ocl_gpu->vec_out);
	profiler.stop(" [CL] finish cleanup");

	return true;
}

/*******************************************************
* FUNCTION: cl_api_exec_mp
* handles OpenCL MP execution
*******************************************************/
bool cl_api_exec_mp(OCL_GPU* ocl_gpu, 
		double* vec_in, 
		double* vec_out, 
		int vec_len, 
		double alfa,
		ARGV_OPTIONS argv_options)
{
	int nthreads, tid;
	const char* cstr_source = NULL;
	string source;
	cl_int err;
	cl_int err_compile;

	/* distribution/mapping of work */
	size_t global_work;
	double retVal;



#pragma omp parallel private(nthreads, tid) shared(ocl_gpu, vec_in, vec_out, vec_len, alfa, argv_options)
{
	tid = omp_get_thread_num();
	
	if(tid == 0){
		cl_device_id cl_devices[NUM_DEVICES];
		cl_platform_id cl_platforms[MAX_PLATFORMS];
		cl_uint cl_num_devices;
		char clStrInfo[MAX_STR_SIZE];
		cl_uint cl_num_platforms;
		cl_num_devices = 0;

		/* used to retrieve SIMD count of CPU/GPU */
		cl_long clDeviceMaxComputeUnits;

		/* get all OpenCL platforms*/
		profiler.stop(" [CL] clGetPlatformIDs");
		clGetPlatformIDs(MAX_PLATFORMS, cl_platforms, &cl_num_platforms);;

		/* get platform info */
		DIE(SELECT_PLATFORM_ID > cl_num_platforms, "invalid platform selected");
		clGetPlatformInfo(cl_platforms[SELECT_PLATFORM_ID],
			CL_PLATFORM_NAME,
			sizeof(char)* MAX_STR_SIZE,
			clStrInfo, NULL);

		/* get device pointers, filter by GPU type */
		profiler.stop(" [CL] clGetDeviceIDs");
		clGetDeviceIDs(cl_platforms[SELECT_PLATFORM_ID],
			CL_DEVICE_TYPE_CPU,
			MAX_DEVICE_COUNT,
			cl_devices,
			&cl_num_devices);

		/* GPU id selected */
		DIE(SELECT_DEVICE_ID > cl_num_devices, "invalid device selected");
		ocl_gpu->dev_id = cl_devices[SELECT_DEVICE_ID];

		/* save device name */
		profiler.stop(" [CL] clGetDeviceInfo");
		clGetDeviceInfo(ocl_gpu->dev_id, CL_DEVICE_NAME,
			sizeof(char)* MAX_STR_SIZE, clStrInfo, NULL);

		printf(" [%s] ", clStrInfo);
		ocl_gpu->info_name = clStrInfo;

		/* get info on architecture MAX_COMPUTE_UNITS */
		clGetDeviceInfo(ocl_gpu->dev_id,
			CL_DEVICE_MAX_COMPUTE_UNITS,
			sizeof(cl_long), &clDeviceMaxComputeUnits, NULL);
		printf("[CU %d]\n", clDeviceMaxComputeUnits);

		/* create context for current gpu */
		profiler.stop(" [CL] clCreateContext");
		ocl_gpu->dev_context = clCreateContext(NULL, 1,
			&ocl_gpu->dev_id, NULL, NULL, NULL);

		/* create queue for current gpu */
		profiler.stop(" [CL] clCreateCommandQueue");
		ocl_gpu->dev_cmd_queue = clCreateCommandQueue(
			ocl_gpu->dev_context,
			ocl_gpu->dev_id, 0, NULL);
	}
	else if(tid == 1){
			/* build OPENCL program against current device */
			profiler.stop(" [USER] user_get_kernel");
			user_get_kernel("kernel.cl", source);
			cstr_source = source.c_str();

		/* display kernel code */
		if (false){
			cout << " --------- BEGIN kernel.cl ----------- " << endl;
			cout << source << endl;
			cout << " --------- END kernel.cl ----------- " << endl;
		}
		/* init problem data*/	
		profiler.stop(" [USER] user_init_data");
		user_init_data(&vec_in, &vec_out, vec_len);
	}

#pragma omp barrier

	if(tid == 0){
		profiler.stop(" [CL] clCreateProgramWithSource");
		ocl_gpu->dev_program = clCreateProgramWithSource(
			ocl_gpu->dev_context, 1, &cstr_source, NULL, &err_compile);
		if(cl_check(err_compile) != true)
			cl_compile_log(ocl_gpu->dev_program, ocl_gpu->dev_id);

		/* compile OpenCL kernel against current GPU */
		profiler.stop(" [CL] clBuildProgram");
		cl_check(clBuildProgram(ocl_gpu->dev_program, 1,
			&ocl_gpu->dev_id, NULL, NULL, NULL));

		/* select compiled kernel exec*/
		profiler.stop(" [CL] clCreateKernel");
		ocl_gpu->dev_kernel_exec = clCreateKernel(
			ocl_gpu->dev_program, "opencl_exec", &err);	
		if(cl_check(err_compile) != true)
			cl_compile_log(ocl_gpu->dev_program, ocl_gpu->dev_id);

		/* allocate memory data in*/
		profiler.stop(" [CL] clCreateBuffer - vec_in");
		ocl_gpu->vec_in = clCreateBuffer(ocl_gpu->dev_context,
			CL_MEM_READ_WRITE,
			vec_len *sizeof(double), NULL, NULL);
		profiler.stop(" [CL] clCreateBuffer - vec_out");
		ocl_gpu->vec_out = clCreateBuffer(ocl_gpu->dev_context,
			CL_MEM_READ_WRITE,
			vec_len *sizeof(double), NULL, NULL);

		/* set return value */
		profiler.stop(" [CL] clEnqueueWriteBuffer - vec_in");
		cl_check(clEnqueueWriteBuffer(ocl_gpu->dev_cmd_queue,
			ocl_gpu->vec_in, CL_TRUE,
			0, vec_len * sizeof(double), &vec_in[0], 0, 0, 0));

		/* Set exec kernel arguments */
		profiler.stop(" [CL] clSetKernelArg");
		cl_check(clSetKernelArg(ocl_gpu->dev_kernel_exec, 0,
			sizeof(cl_mem), (void*)&ocl_gpu->vec_in));
		cl_check(clSetKernelArg(ocl_gpu->dev_kernel_exec, 1,
			sizeof(cl_mem), (void*)&ocl_gpu->vec_out));
		cl_check(clSetKernelArg(ocl_gpu->dev_kernel_exec, 2,
			sizeof(cl_double), (void *)&alfa));
		
		/* inital mapping */
		global_work = vec_len; 

		/* enqueue OpenCL exec*/
		profiler.stop(" [CL] clEnqueueNDRangeKernel");
		cl_check(clEnqueueNDRangeKernel(ocl_gpu->dev_cmd_queue,
			ocl_gpu->dev_kernel_exec, 1, NULL, &global_work,
			0, 0, NULL, NULL));

		/* set return value */
		profiler.stop(" [CL] clEnqueueReadBuffer - vec_out");
		cl_check(clEnqueueReadBuffer(ocl_gpu->dev_cmd_queue,
			ocl_gpu->vec_out, CL_TRUE,
			0, vec_len * sizeof(double), &vec_out[0], 0, NULL, NULL));

		/* free target side memory */
		profiler.stop(" [CL] start cleanup");
		clReleaseKernel(ocl_gpu->dev_kernel_exec);
		clReleaseKernel(ocl_gpu->dev_kernel_exec);
		clReleaseProgram(ocl_gpu->dev_program);
		clReleaseMemObject(ocl_gpu->vec_in);
		clReleaseMemObject(ocl_gpu->vec_out);
		profiler.stop(" [CL] finish cleanup");
	}
}
	return true;
}
