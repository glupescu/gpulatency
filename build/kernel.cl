//#pragma OPENCL EXTENSION cl_khr_fp64 : enable

/* OpenCL main execution function */
__kernel void opencl_exec(__global double* vector_in, 
						__global double* vector_out,
						double alfa)
{
	/* thread index, equivalent of t from initial algorithm */
	int idx = get_global_id(0);
	
	vector_out[idx] = cos(vector_in[idx] * alfa);
}
